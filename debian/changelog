lomiri (0.2.2~butterfly3) lunar; urgency=medium

  * Upstream-provided Debian package for lomiri. See upstream ChangeLog
    for recent changes.

 -- UBports developers <developers@ubports.com>  Fri, 09 Jun 2023 03:24:41 +0200

lomiri (0.1.1) unstable; urgency=medium

  * Upstream-provided Debian package for lomiri. See upstream ChangeLog
    for recent changes.

 -- UBports developers <developers@ubports.com>  Sat, 18 Feb 2023 11:50:01 +0100

lomiri (0.1) unstable; urgency=medium

  * debian/control: Add back telephony as runtime dep

 -- Marius Gripsgard <marius@ubports.com>  Wed, 08 Feb 2023 03:54:13 +0100

lomiri (0.1-2) unstable; urgency=medium

  [ Mike Gabriel ]
  * debian/control: Move lsc to recommends and comment it out for now (Closes:
    #1029292). This issue saw a regression, because upload 0.1-1 was based on
    0.1~git20230111.ba0fc6a-4 instead of 0.1~git20230111.ba0fc6a-5.
  * debian/changelog:
    + Weave-in stanza for lost 0.1~git20230111.ba0fc6a-5.
    + Amend white-space and typos of changelog item in
      0.1~git20230111.ba0fc6a-5.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 07 Feb 2023 22:33:43 +0100

lomiri (0.1-1) unstable; urgency=medium

  * Initial upstream release.
  * debian/watch:
    + Start watching upstream releases.
  * debian/copyright:
    + Update copyright attributions for debian/.
    + Update auto-generated copyright.in file.
    + Update copyright attributions.
  * debian/control:
    + Bump Standards-Version: to 4.6.2. No changes needed.
    + Bump versioned B-D of liblomiri-api-dev to (>= 0.2.0) (which implements
      lomiri-application-shell level 28 required by this version of Lomiri.
      (Closes: #1030439).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 06 Feb 2023 22:01:03 +0100

lomiri (0.1~git20230111.ba0fc6a-5) unstable; urgency=medium

  * debian/control: Move lsc to recommends and comment it out for
    now. (Closes: #1029292).

 -- Marius Gripsgard <marius@ubports.com>  Sun, 22 Jan 2023 07:24:24 +0000

lomiri (0.1~git20230111.ba0fc6a-4) unstable; urgency=medium

  * Upload as is to unstable.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 13 Jan 2023 13:25:30 +0100

lomiri (0.1~git20230111.ba0fc6a-3) experimental; urgency=medium

  * debian/rules:
    + Only adjust permissions in lomiri-tests bin:pkg if it gets built. This
      resolves currently failing arch:all builds on buildd.debian.org.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 12 Jan 2023 23:51:56 +0100

lomiri (0.1~git20230111.ba0fc6a-2) experimental; urgency=medium

  * debian/lomiri-tests: Disable nonmirplugins as this does not
    work with mir2 yet

 -- Marius Gripsgard <marius@ubports.com>  Thu, 12 Jan 2023 14:43:33 +0100

lomiri (0.1~git20230111.ba0fc6a-1) experimental; urgency=medium

  * New Git snapshot (0.1~git20230111.ba0fc6a)
  * debian/patches: Drop upstreamed patches
    + 0001_copyright-headers-Use-uniform-spelling-of-Canonical-.patch
    + 1001_tests-in-libexecdir.patch
    + 1002_build-doc-in-html-subdir.patch
  * debian/control: Add B-D on miroil-dev and miral-dev
  * debian/patches: Add patches to support mir v2.x
    + 0001-Add-support-for-both-older-qtmir-and-newer-qtmir-wit.patch
    + disable-broken-test-mir2.patch
  * debian/control: Bump qtmirserver-dev required version to 0.8.0~

 -- Marius Gripsgard <marius@ubports.com>  Thu, 12 Jan 2023 01:25:14 +0100

lomiri (0.1~git20221008.596f06c-1) experimental; urgency=medium

  * Initial upload to Debian experimental. (Closes: #1023253).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 05 Nov 2022 20:53:26 +0100
