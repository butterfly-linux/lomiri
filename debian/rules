#!/usr/bin/make -f
# -*- makefile -*-

include /usr/share/dpkg/default.mk

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1
export DPKG_GENSYMBOLS_CHECK_LEVEL=4
export NO_PNG_PKG_MANGLE=1
export QT_SELECT=qt5

export DEB_BUILD_MAINT_OPTIONS=hardening=+all
include /usr/share/dpkg/buildflags.mk

DEB_HOST_ARCH ?= $(shell dpkg-architecture -qDEB_HOST_ARCH)

# Skip tests on the archs they are known to be flaky  with current configuration
testskip_architectures := arm64 powerpc ppc64el s390x

%:
	dh $@

override_dh_missing:
	dh_missing --fail-missing

override_dh_auto_configure:
	# Debian defines CMAKE_INSTALL_LOCALSTATEDIR as /usr/var, which is wrong.
	# So until Debian bug 719148 is fixed, do it ourselves.
	dh_auto_configure -- \
		-DCMAKE_INSTALL_LOCALSTATEDIR="/var" \
		-DDISPLAYED_DISTRO_NAME="Debian" \
		$(CONFIGURE_OPTS)

override_dh_auto_build:
	# doc is not a default target
	dh_auto_build -- -O all doc

override_dh_auto_test:
	# Run the entire test battery
	# Parallel tests have a risk of starting xvfb on the same DISPLAY multiple times,
	# causing the whole battery to fail.
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
ifneq (,$(filter $(DEB_HOST_ARCH),$(testskip_architectures)))
	-dh_auto_test --no-parallel -- -O -k xvfballtests
else
	# FIXME: Temporarily disable unit tests on all other architectures, as well. They
	# seem flaky when built+run on systems more recent than Ubuntu 20.04...
	-dh_auto_test --no-parallel -- -O -k xvfballtests
endif
endif

override_dh_fixperms:
	dh_fixperms
	[ -d debian/lomiri-tests ] && chmod a-x debian/lomiri-tests/usr/libexec/lomiri/tests/plugins/Lomiri/Session/libSessionBackendTestPreload.so || true
	[ -d debian/lomiri-tests ] && chmod a-x debian/lomiri-tests/usr/libexec/lomiri/tests/plugins/Lomiri/Launcher/applications/abs-icon.desktop || true
	[ -d debian/lomiri-tests ] && chmod a-x debian/lomiri-tests/usr/libexec/lomiri/tests/plugins/Lomiri/Launcher/applications/click-icon.desktop || true
	[ -d debian/lomiri-tests ] && chmod a-x debian/lomiri-tests/usr/libexec/lomiri/tests/plugins/Lomiri/Launcher/applications/click-icon.svg || true
	[ -d debian/lomiri-tests ] && chmod a-x debian/lomiri-tests/usr/libexec/lomiri/tests/plugins/Lomiri/Launcher/applications/no-name.desktop || true
	[ -d debian/lomiri-tests ] && chmod a-x debian/lomiri-tests/usr/libexec/lomiri/tests/plugins/Lomiri/Launcher/applications/rel-icon.desktop || true
	[ -d debian/lomiri-tests ] && chmod a-x debian/lomiri-tests/usr/libexec/lomiri/tests/plugins/Lomiri/Launcher/applications/rel-icon.svg || true

override_dh_install:
	rm debian/tmp/usr/bin/indicators-client
	rm debian/tmp/usr/share/applications/indicators-client.desktop
	rm debian/tmp/usr/share/lomiri/unlock-device
	cd debian/tmp/usr/share/doc/lomiri/html/ && rdfind -makesymlinks true .
	cd debian/tmp/usr/share/doc/lomiri/html/ && symlinks -rc .
	dh_install

# use private lib directories
override_dh_makeshlibs:
	dh_makeshlibs -Nlomiri-tests

override_dh_shlibdeps:
	# Some mock libraries link against liblightdm-qt5-3.so which we want to
	# avoid, since we only really link against our mock one, not the system one.
	dh_shlibdeps -XlibMockAccountsService-qml.so -Lliblomiri-private0

get-orig-source:
	uscan --noconf --force-download --rename --download-current-version --destdir=..
