# Lao translation for unity8
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the unity8 package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: unity8\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-08 19:08+0000\n"
"PO-Revision-Date: 2023-01-04 18:22+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Lao <https://hosted.weblate.org/projects/lomiri/lomiri/lo/>\n"
"Language: lo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.15.1-dev\n"
"X-Launchpad-Export-Date: 2016-11-23 06:35+0000\n"

#: data/lomiri.desktop.in:4 qml/Components/KeyboardShortcutsOverlay.qml:55
msgid "Lomiri"
msgstr ""

#: data/lomiri.desktop.in:5
msgid "The converged Lomiri shell"
msgstr ""

#: data/lomiri-greeter.desktop.in:4
msgid "Lomiri Greeter"
msgstr ""

#: data/lomiri-greeter.desktop.in:5
msgid "The converged Lomiri shell's Greeter"
msgstr ""

#: data/indicators-client.desktop.in:4
msgid "Indicators Client"
msgstr ""

#: data/indicators-client.desktop.in:5
msgid "Application for testing the Indicators system"
msgstr ""

#: plugins/LightDM/Greeter.cpp:199
msgid "Password: "
msgstr ""

#: plugins/LightDM/Greeter.cpp:214
msgid "Username"
msgstr ""

#: plugins/LightDM/Greeter.cpp:258
msgid "Failed to authenticate"
msgstr ""

#: plugins/LightDM/Greeter.cpp:260
msgid "Invalid password, please try again"
msgstr ""

#: plugins/LightDM/Greeter.cpp:270
msgid "Log In"
msgstr ""

#: plugins/LightDM/Greeter.cpp:270
msgid "Retry"
msgstr ""

#: plugins/LightDM/UsersModel.cpp:157
msgid "Login"
msgstr ""

#: plugins/LightDM/UsersModel.cpp:165
msgid "Guest Session"
msgstr ""

#: plugins/Lomiri/Launcher/launcheritem.cpp:48
#: plugins/Lomiri/Launcher/launcheritem.cpp:122
msgid "Pin shortcut"
msgstr ""

#: plugins/Lomiri/Launcher/launcheritem.cpp:54
msgid "Quit"
msgstr ""

#: plugins/Lomiri/Launcher/launcheritem.cpp:122
msgid "Unpin shortcut"
msgstr ""

#: plugins/WindowManager/Screen.cpp:160
msgid "Unknown"
msgstr ""

#: plugins/WindowManager/Screen.cpp:162
msgid "VGA"
msgstr ""

#: plugins/WindowManager/Screen.cpp:166
msgid "DVI"
msgstr ""

#: plugins/WindowManager/Screen.cpp:168
msgid "Composite"
msgstr ""

#: plugins/WindowManager/Screen.cpp:170
msgid "S-Video"
msgstr ""

#: plugins/WindowManager/Screen.cpp:174
msgid "Internal"
msgstr ""

#: plugins/WindowManager/Screen.cpp:176
msgid "Component"
msgstr ""

#: plugins/WindowManager/Screen.cpp:178
msgid "DisplayPort"
msgstr ""

#: plugins/WindowManager/Screen.cpp:181
msgid "HDMI"
msgstr ""

#: plugins/WindowManager/Screen.cpp:183
msgid "TV"
msgstr ""

#: qml/Components/Dialogs.qml:176
msgctxt "Title: Lock/Log out dialog"
msgid "Log out"
msgstr ""

#: qml/Components/Dialogs.qml:177
msgid "Are you sure you want to log out?"
msgstr ""

#: qml/Components/Dialogs.qml:180
msgctxt "Button: Lock the system"
msgid "Lock"
msgstr ""

#: qml/Components/Dialogs.qml:191
msgctxt "Button: Log out from the system"
msgid "Log Out"
msgstr ""

#: qml/Components/Dialogs.qml:199 qml/Components/Dialogs.qml:284
#: qml/Greeter/NarrowView.qml:218 qml/Wizard/Pages/passcode-confirm.qml:32
#: qml/Wizard/Pages/passcode-set.qml:32
#, fuzzy
msgid "Cancel"
msgstr "Cancel"

#: qml/Components/Dialogs.qml:211
msgctxt "Title: Reboot dialog"
msgid "Reboot"
msgstr ""

#: qml/Components/Dialogs.qml:212
msgid "Are you sure you want to reboot?"
msgstr ""

#: qml/Components/Dialogs.qml:215
msgid "No"
msgstr ""

#: qml/Components/Dialogs.qml:223
msgid "Yes"
msgstr ""

#: qml/Components/Dialogs.qml:243
msgctxt "Title: Power off/Restart dialog"
msgid "Power"
msgstr ""

#: qml/Components/Dialogs.qml:244
msgid ""
"Are you sure you would like\n"
"to power off?"
msgstr ""

#: qml/Components/Dialogs.qml:248
msgctxt "Button: Power off the system"
msgid "Power off"
msgstr ""

#: qml/Components/Dialogs.qml:263
msgctxt "Button: Restart the system"
msgid "Restart"
msgstr ""

#: qml/Components/Dialogs.qml:276
msgid "Screenshot"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:41
msgid "Keyboard Shortcuts"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:62
msgid "PrtScr"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:67
msgid "Takes a screenshot."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:75
msgid "Alt + PrtScr"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:80
msgid "Takes a screenshot of the current window."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:88
msgid "Super + Space"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:93
msgid "Switches to next keyboard layout."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:101
msgid "Super + Shift + Space"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:106
msgid "Switches to previous keyboard layout."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:114
msgid "Ctrl + Alt + T"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:119
msgid "Starts the Terminal application."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:131
msgid "Launcher"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:138
msgid "Super (Hold)"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:143
msgid "Opens the launcher, displays shortcuts."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:151
msgid "Alt + F1"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:156
msgid "Opens launcher keyboard navigation mode."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:164
msgid "Super + Tab"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:169
msgid "Switches applications via the launcher."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:177
msgid "Super + 1 to 0"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:182
msgid "Same as clicking on a launcher icon."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:190
msgid "Super + A"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:195
msgid "Opens the Application Drawer."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:211
msgid "Switching"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:218
msgid "Alt + Tab"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:223
msgid "Switches between applications."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:231
msgid "Super + W"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:236
msgid "Opens the desktop spread."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:244
msgid "Cursor Left or Right"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:249
msgid "Moves the focus."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:261
msgid "Windows"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:268
msgid "Ctrl + Super + D"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:273
msgid "Minimizes all windows."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:281
msgid "Ctrl + Super + Up"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:286
msgid "Maximizes the current window."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:294
msgid "Ctrl + Super + Down"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:299
msgid "Minimizes or restores the current window."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:307
msgid "Ctrl + Super + Left or Right"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:312
msgid "Semi-maximizes the current window."
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:320
msgid "Alt + F4"
msgstr ""

#: qml/Components/KeyboardShortcutsOverlay.qml:325
msgid "Closes the current window."
msgstr ""

#: qml/Components/Lockscreen.qml:214 qml/Greeter/NarrowView.qml:238
msgid "Return to Call"
msgstr ""

#: qml/Components/Lockscreen.qml:214
msgid "Emergency Call"
msgstr ""

#: qml/Components/Lockscreen.qml:247
msgid "OK"
msgstr ""

#: qml/Components/ModeSwitchWarningDialog.qml:33
msgid "Apps may have unsaved data:"
msgstr ""

#: qml/Components/ModeSwitchWarningDialog.qml:60
msgctxt ""
"Re-dock means connect the device again to an external screen/mouse/keyboard"
msgid "Re-dock, save your work and close these apps to continue."
msgstr ""

#: qml/Components/ModeSwitchWarningDialog.qml:67
msgid "Or force close now (unsaved data will be lost)."
msgstr ""

#: qml/Components/ModeSwitchWarningDialog.qml:80
msgid "OK, I will reconnect"
msgstr ""

#: qml/Components/ModeSwitchWarningDialog.qml:81
msgid "Reconnect now!"
msgstr ""

#: qml/Components/ModeSwitchWarningDialog.qml:94
msgid "Close all"
msgstr ""

#: qml/Components/SharingPicker.qml:54
msgid "Preview Share Item"
msgstr ""

#: qml/Components/VirtualTouchPad.qml:312
msgid ""
"Your device is now connected to an external display. Use this screen as a "
"touch pad to interact with the pointer."
msgstr ""

#: qml/Components/VirtualTouchPad.qml:322
msgid "Tap left button to click."
msgstr ""

#: qml/Components/VirtualTouchPad.qml:334
msgid "Tap right button to right click."
msgstr ""

#: qml/Components/VirtualTouchPad.qml:346
msgid "Swipe with two fingers to scroll."
msgstr ""

#: qml/Components/VirtualTouchPad.qml:391
msgid "Find more settings in the system settings."
msgstr ""

#: qml/Greeter/CoverPage.qml:158 tests/qmltests/Greeter/tst_NarrowView.qml:562
msgid "Unlock"
msgstr ""

#: qml/Greeter/DelayedLockscreen.qml:43
msgid "Device Locked"
msgstr ""

#: qml/Greeter/DelayedLockscreen.qml:58
msgid "You have been locked out due to too many failed passphrase attempts."
msgstr ""

#: qml/Greeter/DelayedLockscreen.qml:59
msgid "You have been locked out due to too many failed passcode attempts."
msgstr ""

#: qml/Greeter/DelayedLockscreen.qml:68
#, qt-format
msgid "Please wait %1 minute and then try again…"
msgid_plural "Please wait %1 minutes and then try again…"
msgstr[0] ""
msgstr[1] ""

#: qml/Greeter/Greeter.qml:608 tests/qmltests/Greeter/tst_Greeter.qml:578
#: tests/qmltests/Greeter/tst_Greeter.qml:597
#: tests/qmltests/Greeter/tst_Greeter.qml:625
#: tests/qmltests/Greeter/tst_Greeter.qml:637
msgid "Try again"
msgstr ""

#: qml/Greeter/Greeter.qml:609 tests/qmltests/Greeter/tst_Greeter.qml:552
#: tests/qmltests/Greeter/tst_Greeter.qml:603
msgid "Enter passphrase to unlock"
msgstr ""

#: qml/Greeter/Greeter.qml:610
msgid "Enter passcode to unlock"
msgstr ""

#: qml/Greeter/NarrowView.qml:238
msgid "Emergency"
msgstr ""

#: qml/Greeter/PromptList.qml:143
msgid "Passphrase"
msgstr ""

#: qml/Greeter/PromptList.qml:143
msgid "Passcode"
msgstr ""

#: qml/Greeter/SessionsList.qml:124
msgid "Select desktop environment"
msgstr ""

#: qml/Launcher/Drawer.qml:240
#, fuzzy
#| msgid "Search"
msgid "Search…"
msgstr "ຊອກຫາ"

#: qml/Launcher/PullToRefreshScopeStyle.qml:56
msgid "Pull to refresh…"
msgstr ""

#: qml/Launcher/PullToRefreshScopeStyle.qml:61
msgid "Release to refresh…"
msgstr ""

#: qml/Notifications/NotificationMenuItemFactory.qml:124
msgid "Show password"
msgstr ""

#: qml/Panel/ActiveCallHint.qml:79
msgid "Tap to return to call..."
msgstr ""

#: qml/Panel/ActiveCallHint.qml:93
msgid "Conference"
msgstr ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:936
msgid "Nothing is playing"
msgstr ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:1064
#, qt-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] ""
msgstr[1] ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:1068
#, qt-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] ""
msgstr[1] ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:1073
#, qt-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] ""
msgstr[1] ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:1076
msgid "0 seconds"
msgstr ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:1078
#, qt-format
msgid "%1 remaining"
msgstr ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:1084
msgid "In queue…"
msgstr ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:1088
msgid "Downloading"
msgstr ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:1090
msgid "Paused, tap to resume"
msgstr ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:1092
msgid "Canceled"
msgstr ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:1094
msgid "Finished"
msgstr ""

#: qml/Panel/Indicators/IndicatorMenuItemFactory.qml:1096
msgid "Failed, tap to retry"
msgstr ""

#: qml/Panel/Indicators/MessageMenuItemFactory.qml:151
#: qml/Panel/Indicators/MessageMenuItemFactory.qml:210
msgctxt "Button: Send a reply message"
msgid "Send"
msgstr ""

#: qml/Panel/Indicators/MessageMenuItemFactory.qml:152
msgctxt "Label: Hint in message indicator line edit"
msgid "Reply"
msgstr ""

#: qml/Panel/Indicators/MessageMenuItemFactory.qml:209
msgctxt "Button: Call back on phone"
msgid "Call back"
msgstr ""

#: qml/Panel/PanelMenuPage.qml:92
msgid "Back"
msgstr ""

#: qml/Stage/SideStage.qml:80
msgid "Drag using 3 fingers any application from one window to the other"
msgstr ""

#: qml/Stage/Stage.qml:827
msgid "No running apps"
msgstr ""

#: qml/Tutorial/TutorialLeftLong.qml:49
msgid "Long swipe from the left edge to open the Application Drawer"
msgstr ""

#: qml/Tutorial/TutorialLeft.qml:47
msgid "Short swipe from the left edge to open the launcher"
msgstr ""

#: qml/Tutorial/TutorialRight.qml:54
msgid "Push your mouse against the right edge to view your open apps"
msgstr ""

#: qml/Tutorial/TutorialRight.qml:55
msgid "Swipe from the right edge to view your open apps"
msgstr ""

#: qml/Tutorial/TutorialTop.qml:53
msgid "Swipe from the top edge to access notifications and quick settings"
msgstr ""

#: qml/Wizard/Page.qml:59
msgctxt "Button: Go back one page in the Wizard"
msgid "Back"
msgstr ""

#: qml/Wizard/Pages/10-welcome.qml:29
msgid "Language"
msgstr ""

#: qml/Wizard/Pages/10-welcome.qml:176
#: qml/Wizard/Pages/10-welcome-update.qml:128
#: qml/Wizard/Pages/11-changelog.qml:74 qml/Wizard/Pages/20-keyboard.qml:152
#: qml/Wizard/Pages/30-wifi.qml:213 qml/Wizard/Pages/50-timezone.qml:270
#: qml/Wizard/Pages/60-account.qml:72 qml/Wizard/Pages/70-passwd-type.qml:146
#: qml/Wizard/Pages/passcode-desktop.qml:154
#: qml/Wizard/Pages/password-set.qml:153
msgid "Next"
msgstr ""

#. TRANSLATORS: %1 contains the distro name, %2 the version
#: qml/Wizard/Pages/10-welcome-update.qml:92
#, qt-format
msgid "Welcome to %1 %2"
msgstr ""

#. TRANSLATORS: %1 contains the distro name, %2 the version
#: qml/Wizard/Pages/10-welcome-update.qml:108
#, qt-format
msgid "We will make sure your device is ready to use %1 %2"
msgstr ""

#: qml/Wizard/Pages/11-changelog.qml:28
msgid "What's new"
msgstr ""

#: qml/Wizard/Pages/11-changelog.qml:74
#, fuzzy
msgid "Loading..."
msgstr "ກໍາລັງເວົ້າ"

#: qml/Wizard/Pages/20-keyboard.qml:31
msgid "Select Keyboard"
msgstr ""

#: qml/Wizard/Pages/20-keyboard.qml:71
msgid "Keyboard language"
msgstr ""

#: qml/Wizard/Pages/20-keyboard.qml:93
msgid "Keyboard layout"
msgstr ""

#: qml/Wizard/Pages/20-keyboard.qml:152 qml/Wizard/Pages/30-wifi.qml:213
#: qml/Wizard/Pages/60-account.qml:72 qml/Wizard/Pages/79-system-update.qml:144
#: qml/Wizard/Pages/sim.qml:101
msgid "Skip"
msgstr ""

#: qml/Wizard/Pages/30-wifi.qml:31
msgid "Connect to Wi‑Fi"
msgstr ""

#: qml/Wizard/Pages/30-wifi.qml:139
msgid "Connected"
msgstr ""

#: qml/Wizard/Pages/30-wifi.qml:172
msgid "Available Wi-Fi networks"
msgstr ""

#: qml/Wizard/Pages/30-wifi.qml:173
msgid "No available Wi-Fi networks"
msgstr ""

#: qml/Wizard/Pages/50-timezone.qml:30
msgid "Time Zone"
msgstr ""

#: qml/Wizard/Pages/50-timezone.qml:182
msgid "Enter your city"
msgstr ""

#: qml/Wizard/Pages/60-account.qml:24
msgid "Personalize Your Device"
msgstr ""

#: qml/Wizard/Pages/60-account.qml:54
msgid "Preferred Name"
msgstr ""

#: qml/Wizard/Pages/70-passwd-type.qml:40
msgid "Lock Screen"
msgstr ""

#: qml/Wizard/Pages/70-passwd-type.qml:103
msgctxt "Label: Type of security method"
msgid "Create new password"
msgstr ""

#: qml/Wizard/Pages/70-passwd-type.qml:105
msgctxt "Label: Type of security method"
msgid "Create passcode (numbers only)"
msgstr ""

#: qml/Wizard/Pages/70-passwd-type.qml:107
msgctxt "Label: Type of security method"
msgid "No lock code"
msgstr ""

#: qml/Wizard/Pages/79-system-update.qml:28
msgid "Update Device"
msgstr ""

#: qml/Wizard/Pages/79-system-update.qml:55
msgid ""
"There is a system update available and ready to install. Afterwards, the "
"device will automatically restart."
msgstr ""

#: qml/Wizard/Pages/79-system-update.qml:76
msgctxt "string identifying name of the update"
msgid "Ubuntu Touch system"
msgstr ""

#: qml/Wizard/Pages/79-system-update.qml:83
#, qt-format
msgctxt "version of the system update"
msgid "Version %1"
msgstr ""

#: qml/Wizard/Pages/79-system-update.qml:95
msgid "This could take a few minutes..."
msgstr ""

#: qml/Wizard/Pages/79-system-update.qml:109
msgid "Install and restart now"
msgstr ""

#: qml/Wizard/Pages/80-finished.qml:93
msgid "Welcome Back"
msgstr ""

#. TRANSLATORS: %1 contains the distro name
#: qml/Wizard/Pages/80-finished.qml:95
#, qt-format
msgid "Welcome to %1"
msgstr ""

#: qml/Wizard/Pages/80-finished.qml:111
msgid "You are ready to use your device now"
msgstr ""

#: qml/Wizard/Pages/80-finished.qml:131
msgid "Continue"
msgstr ""

#: qml/Wizard/Pages/80-finished.qml:131
msgid "Get Started"
msgstr ""

#: qml/Wizard/Pages/passcode-confirm.qml:43
#: qml/Wizard/Pages/passcode-desktop.qml:104
msgid "Confirm passcode"
msgstr ""

#: qml/Wizard/Pages/passcode-confirm.qml:45
msgid "Incorrect passcode."
msgstr ""

#: qml/Wizard/Pages/passcode-confirm.qml:45
msgctxt "Enter the passcode again"
msgid "Please re-enter."
msgstr ""

#: qml/Wizard/Pages/passcode-desktop.qml:31
msgid "Lock Screen Passcode"
msgstr ""

#: qml/Wizard/Pages/passcode-desktop.qml:68
msgid "Enter at least 4 numbers to setup your passcode"
msgstr ""

#: qml/Wizard/Pages/passcode-desktop.qml:84
#: qml/Wizard/Pages/passcode-set.qml:54
msgid "Choose passcode"
msgstr ""

#: qml/Wizard/Pages/passcode-desktop.qml:139
msgid "Passcode too short"
msgstr ""

#: qml/Wizard/Pages/passcode-desktop.qml:141
msgid "Passcodes match"
msgstr ""

#: qml/Wizard/Pages/passcode-desktop.qml:143
msgid "Passcodes do not match"
msgstr ""

#: qml/Wizard/Pages/passcode-set.qml:62
msgid "Passcode must be at least 4 characters long"
msgstr ""

#: qml/Wizard/Pages/password-set.qml:31
msgid "Lock Screen Password"
msgstr ""

#: qml/Wizard/Pages/password-set.qml:68
msgid "Enter at least 8 characters"
msgstr ""

#: qml/Wizard/Pages/password-set.qml:80
msgid "Choose password"
msgstr ""

#: qml/Wizard/Pages/password-set.qml:110
msgid "Confirm password"
msgstr ""

#: qml/Wizard/Pages/sim.qml:27
msgid "No SIM card installed"
msgstr ""

#: qml/Wizard/Pages/sim.qml:54
msgid "SIM card added"
msgstr ""

#: qml/Wizard/Pages/sim.qml:55
msgid "You must restart the device to access the mobile network."
msgstr ""

#: qml/Wizard/Pages/sim.qml:59
msgid "Restart"
msgstr ""

#: qml/Wizard/Pages/sim.qml:78
msgid "You won’t be able to make calls or use text messaging without a SIM."
msgstr ""

#: qml/Wizard/Pages/sim.qml:90
msgid "To proceed with no SIM tap Skip."
msgstr ""

#: qml/Wizard/PasswordMeter.qml:87
msgid "Password too short"
msgstr ""

#: qml/Wizard/PasswordMeter.qml:89
msgid "Passwords match"
msgstr ""

#: qml/Wizard/PasswordMeter.qml:91
msgid "Passwords do not match"
msgstr ""

#: qml/Wizard/PasswordMeter.qml:95
msgid "Strong password"
msgstr ""

#: qml/Wizard/PasswordMeter.qml:97
msgid "Fair password"
msgstr ""

#: qml/Wizard/PasswordMeter.qml:99
msgid "Weak password"
msgstr ""

#: qml/Wizard/PasswordMeter.qml:101
msgid "Very weak password"
msgstr ""

#~ msgid "Send"
#~ msgstr "ສົ່ງ"

#~ msgid "Close"
#~ msgstr "ປິດ"

#~ msgid "Well done"
#~ msgstr "ດີຫຼາຍ"

#~ msgid "Confirm"
#~ msgstr "ຢືນຢັນ"

#~ msgid "Speak Now..."
#~ msgstr "ເວົ້າໂລດ"
